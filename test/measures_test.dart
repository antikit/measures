import 'package:measures/measures.dart';
import 'package:test/test.dart';

import '../lib/src/constants.dart';

void main() {
  test('test Time routines', () {
    final time = Time.fromMinutes(15);
    expect(time.min, 15.0);
    expect(time.sec, 900.0);
    expect(time.hrs, 0.25);
    expect(Time.fromDays(1).hrs, 24);
    expect(Time.fromWeeks(1).days, 7);
  });
  test('test Distance routines', () {
    final dist = Distance.fromMetres(926);
    expect(dist.m, 926.0);
    expect(dist.mm, 926000.0);
    expect(dist.cm, 92600.0);
    expect(dist.nm, 0.5);
    expect(dist.km, 0.926);
    expect(Distance.fromIn(1).cm, 2.54);
    expect(Distance.fromMi(1).km, 1.609344);
  });
  test('test Altitude routines', () {
    final alt = Altitude.fromMetres(3048);
    expect(alt.m, 3048.0);
    expect(alt.mGrid50, 3050.0);
    expect(alt.ft, 10000.0);
    expect(alt.ftGrid100, 10100.0);
    expect(alt.FL, 100.0);
    expect(alt.FLGrid5, 105.0);
  });
  test('test Angle routines', () {
    final angle = Angle.fromDeg(270);
    expect(angle.deg, -90.0);
    expect(angle.rad, -pi * 0.5);
  });
  test('test Azimuth routines', () {
    final azm = Azimuth.fromDeg(450);
    expect(azm.deg, 90.0);
    expect(azm.rad, pi * 0.5);
    expect(Azimuth.quadrant.deg, 90.0);
    expect(Azimuth.sextant.deg, 60.0);
    expect(Azimuth.octant.deg, 45.0);
  });
  test('test Mass routines', () {
    final mass = Mass.fromKg(4);
    expect(mass.g, 4000.0);
    expect(mass.tons, 0.004);
    expect(Mass.fromLb(1).g, 453.59237);
    expect(Mass.fromOz(10).g, 283.49523125);
  });
  test('test Pressure routines', () {
    final pressure = Pressure.fromAtm(1);
    expect(pressure.hPa, 1013.25);
    expect(pressure.mmHg, 760.0);
  });
  test('test Speed routines', () {
    final speed = Speed.fromKmh(36);
    expect(speed.ms, 10.0);
    expect(speed.kmMin, 0.6);
    expect(Speed.fromMph(100).kmh, 160.9344);
    expect(Speed.fromKt(10).kmh, 18.52);
    expect(Speed.fromFtMin(1000).ms, 5.08);
  });
  test('test Temperature routines', () {
    final temp = Temperature.fromCelsius(0);
    expect(temp.kelvin, 273.15);
    expect(temp.fahrenheit, 32.0);
  });
  test('test Acceleration routines', () {
    final acceleration = Acceleration.fromG(1);
    expect(acceleration.msSq, 9.80665);
    expect(acceleration.gal, 980.665);
    expect(acceleration.ftSecSq, 32.17404855643044);
  });
  test('test Density routines', () {
    final density = Density.fromKgMCubic(1);
    expect(density.grCmCub, 1000.0);
    expect(density.kgDmCub, 1000.0);
  });
}
