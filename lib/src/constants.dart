import 'dart:math' as math;

// Time
const secPerDay = 86400.0;
const secPerHour = 3600.0;
const secPerMin = 60.0;
const daysPerWeek = 7.0;

// Angular
const pi = math.pi;
const radToDeg = 180.0 / pi;
const degToRad = pi / 180.0;

// Distance
const mPerNm = 1852.0;
const mPerMi = 1609.344;
const mPerFt = 0.3048;
const mPerIn = 0.0254;
const mPerCm = 0.01;
const mPerMm = 0.001;
const mPerKm = 1000.0;

// Speed
const kmhPerKt = 1.852;
const kmhPerMph = 1.609344;
const kmhPerMs = 3.6;
const kmhPerKmMin = 60.0;

// Height
const mPerFL = 30.48;

// Pressure
const hpaPerAtm = 1013.25;
const hpaPerMmHg = hpaPerAtm / 760.0;
const hpaPerInHg = 33.8639;

// Temperature
const tAbs = 273.15;

// Mass
const kgPerLb = 0.45359237;
const kgPerOz = 0.028349523125;
const kgPerGr = 0.001;
const kgPerTon = 1000.0;

// Acceleration
const mPerG = 9.80665;
