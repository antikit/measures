import 'constants.dart';

/// An [Acceleration] intended to deal with acceleration measurements of different nature
/// such as gravitational acceleration and so on.
class Acceleration {
  final double _msSq;

  /// An acceleration with zero value.
  static const Acceleration zero = Acceleration.fromG(0.0);

  /// An acceleration with infinite value.
  static const Acceleration infinite = Acceleration.fromG(double.infinity);

  /// Creates an [Acceleration] instance from [int] or [double] value of metres per second squared.
  const Acceleration.fromMsSq(num msSq) : _msSq = msSq * 1.0;

  /// Creates an [Acceleration] instance from [int] or [double] value of feet per second squared.
  const Acceleration.fromFtSecSq(num ftSecSq) : _msSq = ftSecSq * mPerFt;

  /// Creates an [Acceleration] instance from [int] or [double] value of gal (centimetres per second squared).
  const Acceleration.fromGal(num gal) : _msSq = gal * mPerCm;

  /// Creates an [Acceleration] instance from [int] or [double] value of g (gravitational acceleration).
  const Acceleration.fromG(num g) : _msSq = g * mPerG;

  /// Creates an [Acceleration] instance with NaN value.
  const Acceleration.nan() : _msSq = double.nan;

  /// Returns an [int] hash code for a numerical value of [Acceleration] instance.
  @override
  int get hashCode => _msSq.hashCode;

  /// Returns [Acceleration]'s value in metres per second squared
  double get msSq => _msSq;

  /// Returns [Acceleration]'s value in feet per second squared
  double get ftSecSq => _msSq / mPerFt;

  /// Returns [Acceleration]'s value in gals
  double get gal => _msSq / mPerCm;

  /// Returns [Acceleration]'s value in g (gravitational acceleration)
  double get g => _msSq / mPerG;

  /// Whether the [Acceleration] has the 0.0 value.
  bool get isZero => _msSq == 0.0;

  /// Whether the [Acceleration] has the Not-a-Number value.
  bool get isNaN => _msSq.isNaN;

  /// Whether the [Acceleration] has positive infinity or negative infinity value.
  bool get isInfinite => _msSq.isInfinite;

  /// Whether if the [Acceleration] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _msSq.isNegative;

  /// Returns -1.0 if the [Acceleration] is less than zero,
  /// +1.0 if the [Acceleration] is greater than zero,
  /// and the [Acceleration] itself if it is -0.0, 0.0 or NaN.
  double get sign => _msSq.sign;

  /// String representation of the instance of [Acceleration]
  @override
  String toString() => 'Acceleration: $_msSq 	m/s^2';

  @override
  bool operator ==(Object other) =>
      other is Acceleration && _msSq == other._msSq;
  bool operator >(Acceleration other) => _msSq > other._msSq;
  bool operator >=(Acceleration other) => _msSq >= other._msSq;
  bool operator <(Acceleration other) => _msSq < other._msSq;
  bool operator <=(Acceleration other) => _msSq <= other._msSq;
  Acceleration operator +(Acceleration other) =>
      Acceleration.fromMsSq(_msSq + other._msSq);
  Acceleration operator -(Acceleration other) =>
      Acceleration.fromMsSq(_msSq - other._msSq);
  Acceleration operator *(num other) => Acceleration.fromMsSq(_msSq * other);
  Acceleration operator /(num other) => Acceleration.fromMsSq(_msSq / other);
  Acceleration operator -() => Acceleration.fromMsSq(-_msSq);
}
