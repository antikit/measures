import 'constants.dart';

/// An [Azimuth] intended to deal with measurements of azimuthal angles
/// such as direction/bearing/heading, etc.
///
/// In navigation, bearings or azimuth are measured relative to north.
/// By convention, viewed from above, bearing angles are positive clockwise,
/// so a bearing of 45° corresponds to a north-east orientation.
/// Negative bearings are not used in navigation, so a north-west orientation
/// corresponds to a bearing of 315°.
///
/// When created the [Azimuth] value is normalized between 0.0 and 360.0 degrees.
class Azimuth {
  final double _degrees;

  /// An [Azimuth] with zero value.
  static const Azimuth zero = Azimuth.fromDeg(0.0);

  /// An [Azimuth] instance with 90.0 value.
  static const Azimuth quadrant = Azimuth.fromDeg(90.0);

  /// An [Azimuth] instance with 60.0 value.
  static const Azimuth sextant = Azimuth.fromDeg(60.0);

  /// An [Azimuth] instance with 45.0 value.
  static const Azimuth octant = Azimuth.fromDeg(45.0);

  /// Creates an [Azimuth] instance from [int] or [double] value of degrees.
  const Azimuth.fromDeg(num degrees) : _degrees = (degrees + 360.0) % 360.0;

  /// Creates an [Azimuth] instance from [int] or [double] value of radians.
  factory Azimuth.fromRad(num radians) => Azimuth.fromDeg(radians * radToDeg);

  /// Creates an [Azimuth] instance with NaN value.
  const Azimuth.nan() : _degrees = double.nan;

  /// Returns an [int] hash code for a numerical value of [Azimuth] instance.
  @override
  int get hashCode => _degrees.hashCode;

  /// Returns [Azimuth]'s value in degrees
  double get deg => _degrees;

  /// Returns [Azimuth]'s value in radians
  double get rad => _degrees * degToRad;

  /// Whether the [Azimuth] has the 0.0 value.
  bool get isZero => _degrees == 0.0;

  /// Whether the [Azimuth] has the Not-a-Number value.
  bool get isNaN => _degrees.isNaN;

  /// Whether the [Azimuth] has positive infinity value.
  bool get isInfinite => _degrees.isInfinite;

  /// String representation of the value of [Azimuth]
  String get toStringDeg => '${_degrees.toStringAsFixed(2)}°';

  /// String representation of the instance of [Azimuth]
  String toString() => 'Azimuth: ${_degrees.toStringAsFixed(2)}°';

  @override
  bool operator ==(Object other) =>
      other is Azimuth && _degrees == other._degrees;
  bool operator >(Azimuth other) => _degrees > other._degrees;
  bool operator >=(Azimuth other) => _degrees >= other._degrees;
  bool operator <(Azimuth other) => _degrees < other._degrees;
  bool operator <=(Azimuth other) => _degrees <= other._degrees;
  Azimuth operator +(Azimuth other) =>
      Azimuth.fromDeg(_degrees + other._degrees);
  Azimuth operator -(Azimuth other) =>
      Azimuth.fromDeg(_degrees - other._degrees);
  Azimuth operator *(num other) => Azimuth.fromDeg(_degrees * other);
  Azimuth operator /(num other) => Azimuth.fromDeg(_degrees / other);
}
