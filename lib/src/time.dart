import 'constants.dart';

/// A [Time] intended to deal with measurements of time intervals such as
/// seconds/minutes/hours/days, etc.
class Time {
  final double _seconds;

  /// A [Time] with infinity value.
  static const Time infinite = Time.fromSeconds(double.infinity);

  /// A [Time] with 0.0 value.
  static const Time zero = Time.fromSeconds(0.0);

  /// Creates a [Time] instance from [int] or [double] value of seconds.
  const Time.fromSeconds(num seconds) : _seconds = seconds * 1.0;

  /// Creates a [Time] instance from [int] or [double] value of minutes.
  const Time.fromMinutes(num minutes) : _seconds = minutes * secPerMin;

  /// Creates a [Time] instance from [int] or [double] value of hours.
  const Time.fromHours(num hours) : _seconds = hours * secPerHour;

  /// Creates a [Time] instance from [int] or [double] value of Days.
  const Time.fromDays(num days) : _seconds = days * secPerDay;

  /// Creates a [Time] instance from [int] or [double] value of weeks.
  const Time.fromWeeks(num weeks) : _seconds = weeks * daysPerWeek * secPerDay;

  /// Creates a [Time] instance with NaN value.
  const Time.nan() : _seconds = double.nan;

  /// Returns a [int] hash code for a numerical value of [Time] instance.
  @override
  int get hashCode => _seconds.hashCode;

  /// Returns [Time]'s value in seconds
  double get sec => _seconds;

  /// Returns [Time]'s value in minutes
  double get min => _seconds / secPerMin;

  /// Returns [Time]'s value in hours
  double get hrs => _seconds / secPerHour;

  /// Returns [Time]'s value in days
  double get days => _seconds / secPerDay;

  /// Returns [Time]'s value in weeks
  double get weeks => _seconds / secPerDay / 7.0;

  /// Whether the [Time] has the 0.0 value.
  bool get isZero => _seconds == 0.0;

  /// Whether the [Time] has the Not-a-Number value.
  bool get isNaN => _seconds.isNaN;

  /// Whether the [Time] has positive infinity or negative infinity value.
  bool get isInfinite => _seconds.isInfinite;

  /// Whether if the [Time] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _seconds.isNegative;

  /// Returns -1.0 if the [Time] is less than zero,
  /// +1.0 if the [Time] is greater than zero,
  /// and the [Time] itself if it is -0.0, 0.0 or NaN.
  double get sign => _seconds.sign;

  /// String representation of the instance of [Time]
  @override
  String toString() => 'Time: $_seconds seconds';

  @override
  bool operator ==(Object other) => other is Time && _seconds == other._seconds;
  bool operator >(Time other) => _seconds > other._seconds;
  bool operator >=(Time other) => _seconds >= other._seconds;
  bool operator <(Time other) => _seconds < other._seconds;
  bool operator <=(Time other) => _seconds <= other._seconds;
  Time operator +(Time other) => Time.fromSeconds(_seconds + other._seconds);
  Time operator -(Time other) => Time.fromSeconds(_seconds - other._seconds);
  Time operator *(num other) => Time.fromSeconds(_seconds * other.toDouble());
  Time operator /(num other) => Time.fromSeconds(_seconds / other.toDouble());
}
