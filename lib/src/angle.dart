import 'constants.dart';

/// An [Angle] intended to deal with angular measurements of different nature
/// such as ordinary angles, turn angles, bank, latitude/longitude, etc.
///
/// When created the [Angle] value is normalized between -180.0 and 180.0 degrees.
class Angle {
  final double _degrees;

  /// An [Angle] with zero value.
  static const Angle zero = Angle.fromDeg(0.0);

  /// Creates an [Angle] instance from [int] or [double] value of degrees.
  const Angle.fromDeg(num degrees)
      : _degrees = (degrees + 180.0) % 360.0 < 0.0
            ? (degrees + 180.0) % 360.0 + 180.0
            : (degrees + 180.0) % 360.0 - 180.0;

  /// Creates an [Angle] instance from [int] or [double] value of radians.
  factory Angle.fromRad(num radians) => Angle.fromDeg(radians * radToDeg);

  /// Creates an [Angle] instance with NaN value.
  const Angle.nan() : _degrees = double.nan;

  /// Whether if the [Angle] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _degrees.isNegative;

  /// Returns -1.0 if the [Angle] is less than zero,
  /// +1.0 if the [Angle] is greater than zero,
  /// and the [Angle] itself if it is -0.0, 0.0 or NaN.
  double get sign => _degrees.sign;

  /// Returns an [int] hash code for a numerical value of [Angle] instance.
  @override
  int get hashCode => _degrees.hashCode;

  /// Returns [Angle]'s value in degrees
  double get deg => _degrees;

  /// Returns [Angle]'s value in radians
  double get rad => _degrees * degToRad;

  bool get isNaN => _degrees.isNaN;

  /// String representation of the value of [Angle]
  String get toStringDeg => '${_degrees.toStringAsFixed(2)}°';

  /// String representation of the instance of [Angle]
  String toString() => 'Angle: ${_degrees.toStringAsFixed(2)}°';

  @override
  bool operator ==(Object other) =>
      other is Angle && _degrees == other._degrees;
  bool operator >(Angle other) => _degrees > other._degrees;
  bool operator >=(Angle other) => _degrees >= other._degrees;
  bool operator <(Angle other) => _degrees < other._degrees;
  bool operator <=(Angle other) => _degrees <= other._degrees;
  Angle operator +(Angle other) => Angle.fromDeg(_degrees + other._degrees);
  Angle operator -(Angle other) => Angle.fromDeg(_degrees - other._degrees);
  Angle operator *(num other) => Angle.fromDeg(_degrees * other);
  Angle operator /(num other) => Angle.fromDeg(_degrees / other);
  Angle operator -() => Angle.fromDeg(-_degrees);
}
