import 'constants.dart';

/// A [Temperature] intended to deal with temperature measurements in a various
/// scales such as Celsius, Fahrenheit and Kelvin.
class Temperature {
  final double _celsius;

  /// A [Temperature] with infinity value.
  static const Temperature infinite = Temperature.fromCelsius(double.infinity);

  /// A [Temperature] with 0.0°C value.
  static const Temperature zero_C = Temperature.fromCelsius(0.0);

  /// A [Temperature] with 0.0°F value.
  static const Temperature zero_F = Temperature.fromFahrenheit(0.0);

  /// A [Temperature] with 0.0°K value.
  static const Temperature zero_K = Temperature.fromKelvin(0.0);

  /// Creates a [Temperature] instance from [int] or [double] value of Celsius degrees.
  const Temperature.fromCelsius(num tempCelsius) : _celsius = tempCelsius * 1.0;

  /// Creates a [Temperature] instance from [int] or [double] value of Kelvin degrees.
  const Temperature.fromKelvin(num tempKelvin) : _celsius = tempKelvin - tAbs;

  /// Creates a [Temperature] instance from [int] or [double] value of Fahrenheit degrees.
  const Temperature.fromFahrenheit(num tempFahrenheit)
      : _celsius = (tempFahrenheit - 32.0) * 5.0 / 9.0;

  /// Creates a [Temperature] instance with NaN value.
  const Temperature.nan() : _celsius = double.nan;

  /// Returns an [int] hash code for a numerical value of [Temperature] instance.
  @override
  int get hashCode => _celsius.hashCode;

  /// Returns [Temperature]'s value in Celsius degrees
  double get celsius => _celsius;

  /// Returns [Temperature]'s value in Kelvin degrees
  double get kelvin => _celsius + tAbs;

  /// Returns [Temperature]'s value in Fahrenheit degrees
  double get fahrenheit => _toFahrenheit(_celsius);

  /// Whether the [Temperature] has the Not-a-Number value.
  bool get isNaN => _celsius.isNaN;

  /// Whether the [Temperature] has positive infinity or negative infinity value.
  bool get isInfinite => _celsius.isInfinite;

  /// String representation of the value of [Temperature]
  String get toStringC => '${_celsius.toStringAsFixed(1)}°';
  String get toStringK => '${(_celsius + tAbs).toStringAsFixed(1)}°';
  String get toStringF => '${_toFahrenheit(_celsius).toStringAsFixed(1)}°';

  /// String representation of the instance of [Temperature]
  @override
  String toString() => 'Temperature: ${_celsius.toStringAsFixed(1)}°';

  @override
  bool operator ==(Object other) =>
      other is Temperature && _celsius == other._celsius;
  bool operator >(Temperature other) => _celsius > other._celsius;
  bool operator >=(Temperature other) => _celsius >= other._celsius;
  bool operator <(Temperature other) => _celsius < other._celsius;
  bool operator <=(Temperature other) => _celsius <= other._celsius;
  Temperature operator +(Temperature other) =>
      Temperature.fromCelsius(_celsius + other._celsius);
  Temperature operator -(Temperature other) =>
      Temperature.fromCelsius(_celsius - other._celsius);
  Temperature operator *(num other) =>
      Temperature.fromCelsius(_celsius * other);
  Temperature operator /(num other) =>
      Temperature.fromCelsius(_celsius / other);

  static double _toFahrenheit(num temp) => temp * 9.0 / 5.0 + 32.0;
}
