import 'constants.dart';

/// An [Altitude] intended to deal with vertical measurements such as
/// height/altitude/elevation and flight level (FL), mostly in aviation and
/// aeronautical calculations.
class Altitude {
  final double _metres;

  /// An altitude with zero value.
  static const Altitude zero = Altitude.fromMetres(0.0);

  /// An altitude with infinite value.
  static const Altitude infinite = Altitude.fromMetres(double.infinity);

  /// Creates an [Altitude] instance from [int] or [double] value of meters.
  const Altitude.fromMetres(num metres) : _metres = metres * 1.0;

  /// Creates an [Altitude] instance from [int] or [double] value of feet.
  const Altitude.fromFt(num feet) : _metres = feet * mPerFt;

  /// Creates an [Altitude] instance from [int] or [double] value of Flight Level (FL).
  const Altitude.fromFL(num flightLvl) : _metres = flightLvl * mPerFL;

  /// Creates an [Altitude] instance with NaN value.
  const Altitude.nan() : _metres = double.nan;

  /// Returns an [int] hash code for a numerical value of [Altitude] instance.
  @override
  int get hashCode => _metres.hashCode;

  /// Returns [Altitude]'s value in metres
  double get m => _metres;

  /// Returns [Altitude]'s value in metres rounded up to fifty metres
  double get mGrid50 => (_metres / 50.0 + 1.0).truncateToDouble() * 50.0;

  /// Returns [Altitude]'s value in feet
  double get ft => _metres / mPerFt;

  /// Returns [Altitude]'s value in feet rounded up to hundred of feet
  double get ftGrid100 => (_metres / mPerFL + 1.0).truncateToDouble() * 100.0;

  /// Returns [Altitude]'s value in FL (Flight Levels) rounded to the nearest
  /// integer number
  double get FL => (_metres / mPerFL + 0.5).truncateToDouble();

  /// Returns [Altitude]'s value in FL (Flight Levels) rounded up to the nearest
  /// number multiple of five
  double get FLGrid5 => (FL / 5.0 + 1.0).truncateToDouble() * 5.0;

  /// Whether the [Altitude] has the 0.0 value.
  bool get isZero => _metres == 0.0;

  /// Whether the [Altitude] has the Not-a-Number value.
  bool get isNaN => _metres.isNaN;

  /// Whether the [Altitude] has positive infinity or negative infinity value.
  bool get isInfinite => _metres.isInfinite;

  /// Whether if the [Altitude] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _metres.isNegative;

  /// Returns -1.0 if the [Altitude] is less than zero,
  /// +1.0 if the [Altitude] is greater than zero,
  /// and the [Altitude] itself if it is -0.0, 0.0 or NaN.
  double get sign => _metres.sign;

  /// String representation of the instance of [Altitude]
  @override
  String toString() => 'Altitude: $_metres metres';

  @override
  bool operator ==(Object other) =>
      other is Altitude && _metres == other._metres;
  bool operator >(Altitude other) => _metres > other._metres;
  bool operator >=(Altitude other) => _metres >= other._metres;
  bool operator <(Altitude other) => _metres < other._metres;
  bool operator <=(Altitude other) => _metres <= other._metres;
  Altitude operator +(Altitude other) =>
      Altitude.fromMetres(_metres + other._metres);
  Altitude operator -(Altitude other) =>
      Altitude.fromMetres(_metres - other._metres);
  Altitude operator *(num other) => Altitude.fromMetres(_metres * other);
  Altitude operator /(num other) => Altitude.fromMetres(_metres / other);
  Altitude operator -() => Altitude.fromMetres(-_metres);
}
