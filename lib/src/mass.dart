import 'constants.dart';

/// A [Mass] intended to deal with mass.
class Mass {
  final double _kg;

  /// A [Mass] with infinity value.
  static const Mass infinite = Mass.fromGr(double.infinity);

  /// A [Mass] with 0.0 value.
  static const Mass zero = Mass.fromGr(0.0);

  /// Creates a [Mass] instance from [int] or [double] value of kilograms.
  const Mass.fromKg(num kilograms) : _kg = kilograms * 1.0;

  /// Creates a [Mass] instance from [int] or [double] value of grams.
  const Mass.fromGr(num grams) : _kg = grams * kgPerGr;

  /// Creates a [Mass] instance from [int] or [double] value of tons.
  const Mass.fromTon(num tons) : _kg = tons * kgPerTon;

  /// Creates a [Mass] instance from [int] or [double] value of pounds.
  const Mass.fromLb(num pounds) : _kg = pounds * kgPerLb;

  /// Creates a [Mass] instance from [int] or [double] value of ounces.
  const Mass.fromOz(num ounces) : _kg = ounces * kgPerOz;

  /// Creates a [Mass] instance with NaN value.
  const Mass.nan() : _kg = double.nan;

  /// Returns an [int] hash code for a numerical value of [Mass] instance.
  @override
  int get hashCode => _kg.hashCode;

  /// Returns [Mass]'s value in kilograms
  double get kg => _kg;

  /// Returns [Mass]'s value in grams
  double get g => _kg / kgPerGr;

  /// Returns [Mass]'s value in tons
  double get tons => _kg / kgPerTon;

  /// Returns [Mass]'s value in pounds
  double get lb => _kg / kgPerLb;

  /// Returns [Mass]'s value in ounces
  double get oz => _kg / kgPerOz;

  /// Whether the [Mass] has the 0.0 value.
  bool get isZero => _kg == 0.0;

  /// Whether the [Mass] has the Not-a-Number value.
  bool get isNaN => _kg.isNaN;

  /// Whether the [Mass] has positive infinity or negative infinity value.
  bool get isInfinite => _kg.isInfinite;

  /// Whether if the [Mass] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _kg.isNegative;

  /// Returns -1.0 if the [Mass] is less than zero,
  /// +1.0 if the [Mass] is greater than zero,
  /// and the [Mass] itself if it is -0.0, 0.0 or NaN.
  double get sign => _kg.sign;

  /// String representation of the instance of [Mass]
  @override
  String toString() => 'Mass: $_kg kg';

  @override
  bool operator ==(Object other) => other is Mass && _kg == other._kg;
  bool operator >(Mass other) => _kg > other._kg;
  bool operator >=(Mass other) => _kg >= other._kg;
  bool operator <(Mass other) => _kg < other._kg;
  bool operator <=(Mass other) => _kg <= other._kg;
  Mass operator +(Mass other) => Mass.fromKg(_kg + other._kg);
  Mass operator -(Mass other) => Mass.fromKg(_kg - other._kg);
  Mass operator *(num other) => Mass.fromKg(_kg * other);
  Mass operator /(num other) => Mass.fromKg(_kg / other);
}
