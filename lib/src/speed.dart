import 'constants.dart';

/// A [Speed] intended to deal with speed measurements of different nature
/// such as ground speed and airspeed in aviation and aeronautical calculations.
class Speed {
  final double _kmh;

  /// A [Speed] with infinity value.
  static const Speed infinite = Speed.fromMs(double.infinity);

  /// A [Speed] with 0.0 value.
  static const Speed zero = Speed.fromMs(0.0);

  /// Creates a [Speed] instance from [int] or [double] value of kilometres per hour.
  const Speed.fromKmh(num kmh) : _kmh = kmh * 1.0;

  /// Creates a [Speed] instance from [int] or [double] value of knots (or nautical miles per hour).
  const Speed.fromKt(num knots) : _kmh = knots * kmhPerKt;

  /// Creates a [Speed] instance from [int] or [double] value of miles per hour.
  const Speed.fromMph(num mph) : _kmh = mph * kmhPerMph;

  /// Creates a [Speed] instance from [int] or [double] value of metres per second.
  const Speed.fromMs(num ms) : _kmh = ms * kmhPerMs;

  /// Creates a [Speed] instance from [int] or [double] value of kilometres per minute.
  const Speed.fromKmMin(num kmMin) : _kmh = kmMin * kmhPerKmMin;

  /// Creates a [Speed] instance from [int] or [double] value of feet per minute.
  const Speed.fromFtMin(num ftMin)
      : _kmh = ftMin * mPerFt * kmhPerKmMin / mPerKm;

  /// Creates a [Speed] instance with NaN value.
  const Speed.nan() : _kmh = double.nan;

  /// Returns an [int] hash code for a numerical value of [Speed] instance.
  @override
  int get hashCode => _kmh.hashCode;

  /// Returns [Speed]'s value in kilometres per hour
  double get kmh => _kmh;

  /// Returns [Speed]'s value in knots (nautical mile per hour)
  double get kt => _kmh / kmhPerKt;

  /// Returns [Speed]'s value in (statute) mile per hour
  double get mph => _kmh / kmhPerMph;

  /// Returns [Speed]'s value in metres per second
  double get ms => _kmh / kmhPerMs;

  /// Returns [Speed]'s value in kilometres per minute
  double get kmMin => _kmh / kmhPerKmMin;

  /// Returns [Speed]'s value in feet per minute
  double get ftMin => _kmh / kmhPerKmMin * mPerKm / mPerFt;

  /// Whether the [Speed] has the 0.0 value.
  bool get isZero => _kmh == 0.0;

  /// Whether the [Speed] has the Not-a-Number value.
  bool get isNaN => _kmh.isNaN;

  /// Whether the [Speed] has positive infinity or negative infinity value.
  bool get isInfinite => _kmh.isInfinite;

  /// Whether if the [Speed] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _kmh.isNegative;

  /// Returns -1.0 if the [Speed] is less than zero,
  /// +1.0 if the [Speed] is greater than zero,
  /// and the [Speed] itself if it is -0.0, 0.0 or NaN.
  double get sign => _kmh.sign;

  /// String representation of the instance of [Speed]
  @override
  String toString() => 'Speed: $_kmh kmh';

  @override
  bool operator ==(Object other) => other is Speed && _kmh == other._kmh;
  bool operator >(Speed other) => _kmh > other._kmh;
  bool operator >=(Speed other) => _kmh >= other._kmh;
  bool operator <(Speed other) => _kmh < other._kmh;
  bool operator <=(Speed other) => _kmh <= other._kmh;
  Speed operator +(Speed other) => Speed.fromKmh(_kmh + other._kmh);
  Speed operator -(Speed other) => Speed.fromKmh(_kmh - other._kmh);
  Speed operator *(num other) => Speed.fromKmh(_kmh * other);
  Speed operator /(num other) => Speed.fromKmh(_kmh / other);
}
