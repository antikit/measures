import 'constants.dart';

/// A [Pressure] intended to deal with pressure measurements, for example
/// atmospheric pressure.
class Pressure {
  final double _hpa;

  /// A [Pressure] with infinity value.
  static const Pressure infinite = Pressure.fromHpa(double.infinity);

  /// A [Pressure] with 0.0 value.
  static const Pressure zero = Pressure.fromHpa(0.0);

  /// Creates a [Pressure] instance from [int] or [double] value of hectopascals.
  const Pressure.fromHpa(num hPa) : _hpa = hPa * 1.0;

  /// Creates a [Pressure] instance from [int] or [double] value of millimetres of mercury.
  const Pressure.fromMmHg(num mmHg) : _hpa = mmHg * hpaPerMmHg;

  /// Creates a [Pressure] instance from [int] or [double] value of inches of mercury.
  const Pressure.fromInHg(num inHg) : _hpa = inHg * hpaPerInHg;

  /// Creates a [Pressure] instance from [int] or [double] value of standard atmosphere.
  const Pressure.fromAtm(num atm) : _hpa = atm * hpaPerAtm;

  /// Creates a [Pressure] instance with NaN value.
  const Pressure.nan() : _hpa = double.nan;

  /// Returns an [int] hash code for a numerical value of [Pressure] instance.
  @override
  int get hashCode => _hpa.hashCode;

  /// Returns [Pressure]'s value in hectopascals
  double get hPa => _hpa;

  /// Returns [Pressure]'s value in millimetres of mercury
  double get mmHg => _hpa / hpaPerMmHg;

  /// Returns [Pressure]'s value in inches of mercury
  double get inHg => _hpa / hpaPerInHg;

  /// Returns [Pressure]'s value in standard atmospheres
  double get atm => _hpa / hpaPerAtm;

  /// Whether the [Pressure] has the 0.0 value.
  bool get isZero => _hpa == 0.0;

  /// Whether the [Pressure] has the Not-a-Number value.
  bool get isNaN => _hpa.isNaN;

  /// Whether the [Pressure] has positive infinity or negative infinity value.
  bool get isInfinite => _hpa.isInfinite;

  /// Whether if the [Pressure] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _hpa.isNegative;

  /// Returns -1.0 if the [Pressure] is less than zero,
  /// +1.0 if the [Pressure] is greater than zero,
  /// and the [Pressure] itself if it is -0.0, 0.0 or NaN.
  double get sign => _hpa.sign;

  /// String representation of the instance of [Pressure]
  @override
  String toString() => 'Pressure: $_hpa hPa';

  @override
  bool operator ==(Object other) => other is Pressure && _hpa == other._hpa;
  bool operator >(Pressure other) => _hpa > other._hpa;
  bool operator >=(Pressure other) => _hpa >= other._hpa;
  bool operator <(Pressure other) => _hpa < other._hpa;
  bool operator <=(Pressure other) => _hpa <= other._hpa;
  Pressure operator +(Pressure other) => Pressure.fromHpa(_hpa + other._hpa);
  Pressure operator -(Pressure other) => Pressure.fromHpa(_hpa - other._hpa);
  Pressure operator *(num other) => Pressure.fromHpa(_hpa * other);
  Pressure operator /(num other) => Pressure.fromHpa(_hpa / other);
}
