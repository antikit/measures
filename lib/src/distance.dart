import 'constants.dart';

/// A [Distance] intended to deal with horizontal measurements such as
/// length/range/distance, etc.
class Distance {
  final double _metres;

  /// A [Distance] with infinity value.
  static const Distance infinite = Distance.fromMetres(double.infinity);

  /// A [Distance] with 0.0 value.
  static const Distance zero = Distance.fromMetres(0.0);

  /// Creates a [Distance] instance from [int] or [double] value of meters.
  const Distance.fromMetres(num metres) : _metres = metres * 1.0;

  /// Creates a [Distance] instance from [int] or [double] value of millimetres.
  const Distance.fromMm(num millimetres) : _metres = millimetres * mPerMm;

  /// Creates a [Distance] instance from [int] or [double] value of centimetres.
  const Distance.fromSm(num centimetres) : _metres = centimetres * mPerCm;

  /// Creates a [Distance] instance from [int] or [double] value of inches.
  const Distance.fromIn(num inches) : _metres = inches * mPerIn;

  /// Creates a [Distance] instance from [int] or [double] value of feet.
  const Distance.fromFt(num feet) : _metres = feet * mPerFt;

  /// Creates a [Distance] instance from [int] or [double] value of kilometres.
  const Distance.fromKm(num kilometres) : _metres = kilometres * mPerKm;

  /// Creates a [Distance] instance from [int] or [double] value of nautical miles.
  const Distance.fromNm(num nauticalMiles) : _metres = nauticalMiles * mPerNm;

  /// Creates a [Distance] instance from [int] or [double] value of statute miles.
  const Distance.fromMi(num statuteMiles) : _metres = statuteMiles * mPerMi;

  /// Creates a [Distance] instance with NaN value.
  const Distance.nan() : _metres = double.nan;

  /// Returns a [int] hash code for a numerical value of [Distance] instance.
  @override
  int get hashCode => _metres.hashCode;

  /// Returns [Distance]'s value in metres
  double get m => _metres;

  /// Returns [Distance]'s value in millimetres
  double get mm => _metres / mPerMm;

  /// Returns [Distance]'s value in centimetres
  double get cm => _metres / mPerCm;

  /// Returns [Distance]'s value in inches
  double get inch => _metres / mPerIn;

  /// Returns [Distance]'s value in feet
  double get ft => _metres / mPerFt;

  /// Returns [Distance]'s value in kilometres
  double get km => _metres / mPerKm;

  /// Returns [Distance]'s value in nautical miles
  double get nm => _metres / mPerNm;

  /// Returns [Distance]'s value in statute miles
  double get mi => _metres / mPerMi;

  /// Whether the [Distance] has the 0.0 value.
  bool get isZero => _metres == 0.0;

  /// Whether the [Distance] has the Not-a-Number value.
  bool get isNaN => _metres.isNaN;

  /// Whether the [Distance] has positive infinity or negative infinity value.
  bool get isInfinite => _metres.isInfinite;

  /// Whether if the [Distance] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _metres.isNegative;

  /// Returns -1.0 if the [Distance] is less than zero,
  /// +1.0 if the [Distance] is greater than zero,
  /// and the [Distance] itself if it is -0.0, 0.0 or NaN.
  double get sign => _metres.sign;

  /// String representation of the instance of [Distance]
  @override
  String toString() => 'Distance: $_metres metres';

  @override
  bool operator ==(Object other) =>
      other is Distance && _metres == other._metres;
  bool operator >(Distance other) => _metres > other._metres;
  bool operator >=(Distance other) => _metres >= other._metres;
  bool operator <(Distance other) => _metres < other._metres;
  bool operator <=(Distance other) => _metres <= other._metres;
  Distance operator +(Distance other) =>
      Distance.fromMetres(_metres + other._metres);
  Distance operator -(Distance other) =>
      Distance.fromMetres(_metres - other._metres);
  Distance operator *(num other) => Distance.fromMetres(_metres * other);
  Distance operator /(num other) => Distance.fromMetres(_metres / other);
}
