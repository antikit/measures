/// A [Density] intended to deal with density of material.
class Density {
  final double _kgMCub;

  /// A [Density] with infinity value.
  static const Density infinite = Density.fromGrCmCubic(double.infinity);

  /// A [Density] with 0.0 value.
  static const Density zero = Density.fromGrCmCubic(0.0);

  /// Creates a [Density] instance from [int] or [double] value of kilogram per cubic metre.
  const Density.fromKgMCubic(num kgMCubic) : _kgMCub = kgMCubic * 1.0;

  /// Creates a [Density] instance from [int] or [double] value of kilogram per cubic decimetre (litre).
  const Density.fromKgDmCubic(num kgDmCub) : _kgMCub = kgDmCub * 0.001;

  /// Creates a [Density] instance from [int] or [double] value of gram per cubic centimetre.
  const Density.fromGrCmCubic(num grCmCub) : _kgMCub = grCmCub * 0.001;

  /// Creates a [Density] instance with NaN value.
  const Density.nan() : _kgMCub = double.nan;

  /// Returns an [int] hash code for a numerical value of [Density] instance.
  @override
  int get hashCode => _kgMCub.hashCode;

  /// Returns [Density]'s value in kilograms per cubic metre
  double get kgMCub => _kgMCub;

  /// Returns [Density]'s value in kilograms per cubic decimetre (litre)
  double get kgDmCub => _kgMCub * 1000.0;

  /// Returns [Density]'s value in grams per cubic centimetre
  double get grCmCub => _kgMCub * 1000.0;

  /// Whether the [Density] has the 0.0 value.
  bool get isZero => _kgMCub == 0.0;

  /// Whether the [Density] has the Not-a-Number value.
  bool get isNaN => _kgMCub.isNaN;

  /// Whether the [Density] has positive infinity or negative infinity value.
  bool get isInfinite => _kgMCub.isInfinite;

  /// Whether if the [Density] is negative.
  /// Negative values are those less than zero, and the `-0.0`.
  bool get isNegative => _kgMCub.isNegative;

  /// Returns -1.0 if the [Density] is less than zero,
  /// +1.0 if the [Density] is greater than zero,
  /// and the [Density] itself if it is -0.0, 0.0 or NaN.
  double get sign => _kgMCub.sign;

  /// String representation of the instance of [Density]
  @override
  String toString() => 'Density: $_kgMCub kgMCubic';

  @override
  bool operator ==(Object other) =>
      other is Density && _kgMCub == other._kgMCub;
  bool operator >(Density other) => _kgMCub > other._kgMCub;
  bool operator >=(Density other) => _kgMCub >= other._kgMCub;
  bool operator <(Density other) => _kgMCub < other._kgMCub;
  bool operator <=(Density other) => _kgMCub <= other._kgMCub;
  Density operator +(Density other) =>
      Density.fromKgMCubic(_kgMCub + other._kgMCub);
  Density operator -(Density other) =>
      Density.fromKgMCubic(_kgMCub - other._kgMCub);
  Density operator *(num other) => Density.fromKgMCubic(_kgMCub * other);
  Density operator /(num other) => Density.fromKgMCubic(_kgMCub / other);
}
