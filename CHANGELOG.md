# CHANGELOG
Any notable changes to this project will be documented in this file.
This project uses [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

## 1.0.0
* First version release.
* zero and infinite constants added.

## 0.2.2
* Description of package amended and extended.

## 0.2.0
* Density class added, few typos removed.

## 0.1.0
* Acceleration class added.

## 0.0.2
* Description for classes added, some improvements, dependency from meta removed, few bug fixes.

## 0.0.1
* Initial development release.
