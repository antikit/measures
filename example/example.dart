// Copyright (c) 2021, Anton Antonchik.  All rights reserved. Use of this source
// code is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:measures/measures.dart';

main() {
  var dist = Distance.fromNm(10); // Set the value 10 nautical miles
  print(dist.km); // Get the value in kilometres: 18.52 km
  print(dist.m); // Get the value in metres: 18520.0 m

  var speed = Speed.fromKt(100); // Set the value 100 knots
  print(speed.kmh); // Get the value in kilometres per hour: 185.2 km/h
  print(speed.ms); // Get the value in metres per second: 51.444 m/s

  var temp = Temperature.fromCelsius(15); // Set the value 15° C
  print(temp.fahrenheit); // Get the value in degrees Fahrenheit: 59.0° F
  print(temp.kelvin); // Get the value in degrees Fahrenheit: 288.15° K

  var altitude1 = Altitude.zero; // Set the value 0.0
  var altitude2 = Altitude.fromMetres(50); // Set the value 50.0 m
  var altitude3 = Altitude.fromFt(100); // Set the value 100.0 ft
  var altitude4 = altitude2 + altitude3; // Add or distract them
  var altitude5 = altitude4 * 10.0; // Multiply or divide them
  var isHigher = altitude2 > altitude1; // Compare them
  print(isHigher); // true
  print(altitude5.m); // Get the value in metres: 804.8 m
}
