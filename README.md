# measures

[![Pub Version](https://img.shields.io/pub/v/measures?logo=dart&logoColor=white)](https://pub.dev/packages/measures)
![Dart Platform](https://badgen.net/pub/dart-platform/measures)
![Flutter Platform](https://badgen.net/pub/flutter-platform/measures)
![SDK](https://badgen.net/pub/sdk-version/measures)
![License](https://badgen.net/pub/license/measures)
[![Pub Likes](https://badgen.net/pub/likes/measures)](https://pub.dev/packages/measures/score)
[![Pub points](https://badgen.net/pub/points/measures)](https://pub.dev/packages/measures/score)
[![Pub popularity](https://badgen.net/pub/popularity/measures)](https://pub.dev/packages/measures/score)

A simple but useful package that won't let you be confused with what exactly units are you using in a particular place and get the necessary one in an easy way.

## About

The package intended to help developers comfortably handle routine operations with commonly used units of measure, provide seamless units conversions and to improve readability and unambiguity of code.

## Currently supported features

At the moment following measure types are supported:
- Acceleration;
- Altitude;
- Angle;
- Azimuth;
- Density;
- Distance;
- Mass;
- Pressure;
- Speed;
- Temperature;
- Time.

## Overview

## Usage

### Basic use case

#### Use corresponding constructor to pass a value in origin units, and then get it in units you need when you need. For example:

```dart
import 'package:measures/measures.dart';

main() {
  var dist = Distance.fromNm(10); // Set the value 10 nautical miles
  print(dist.km); // Get the value in kilometres: 18.52 km
  print(dist.m); // Get the value in metres: 18520.0 m
}
```
## Installation

Add measures to your `pubspec.yaml` file:

```yaml
dependencies:
  measures: ^1.0.0
```

## Changelog

All notable changes to this project will be documented in [this file](./CHANGELOG.md).

## Issues

For issues, file directly in the [measures repo](https://bitbucket.org/antikit/measures).

## License

[The 3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause)